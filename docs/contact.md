# Contact

You can create [issues](https://gitlab.com/wurssb/NG-Tax/-/issues) or [contact](mailto:jasper.koehorst@wur.nl) us directly.
