# Coverage

## Summary

The coverage functionality in NG-Tax allows users to generate an overview for which taxa can be targeted given a primer subset. 

*Please not that this is a recent addition to NG-Tax and only available from **2.2.17** onwards.*

To use this function start with `-coverage` followed by the primer set used, the reference database either in alignment or fasta format and the length you would like to use. Depending on the size of the database it can take a moment to process all the entires.

```bash
    -coverage
    -for_p ACTCCTACGGRAGGCAGCA
    -rev_p GACTACHVGGGTWTCTAAT
    -refdb ./databases/SILVA_138_SSURef_tax_silva.subset.fasta.gz
    -for_read_len 70
    -rev_read_len 70
```

Once finished a `coverage.tsv` is generated that looks like (normally it is tab separated):

```bash
Superkingdom,Phylum,Class,Order,Family,Genus,Species,Hit,Total
Bacteria,,,,,,,67237,70303
Bacteria,Abditibacteriota,,,,,,1,1
Bacteria,Abditibacteriota,Abditibacteria,,,,,1,1
Bacteria,Abditibacteriota,Abditibacteria,Abditibacteriales,,,,1,1
Bacteria,Abditibacteriota,Abditibacteria,Abditibacteriales,Abditibacteriaceae,,,1,1
Bacteria,Abditibacteriota,Abditibacteria,Abditibacteriales,Abditibacteriaceae,Abditibacterium,,1,1
Bacteria,Abditibacteriota,Abditibacteria,Abditibacteriales,Abditibacteriaceae,Abditibacterium,bacterium LY17,1,1
Bacteria,Acidobacteriota,,,,,,53,53
...
```

As you can see in this example there are 70303 bacterial entries for which 67237 had a match with the primer set.
When scrolling through the list you can see which taxa are either completely missed, partially covered or completely covered.
This is shown in the last 2 columns. Number of hits and the total number of entries.

## Visualization

At the moment it is a straight forward table that you should be able to load into Excel or other software that can easily read tab delimited files.
If you have any ideas on how to visualize this further please let us know!