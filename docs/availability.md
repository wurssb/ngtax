# Availability 

## NG-Tax 2.0

* Galaxy docker ([see NG-Tax galaxy tutorial section for more details](galaxy))

* Standalone JAR file: http://download.systemsbiology.nl/ngtax ([see NG-Tax commandline tutorial for more details ](commandLine))

* Git repository: https://gitlab.com/wurssb/NG-Tax (also contains galaxy xml files)

## Toolbox

<!-- * Toolbox docker ([see Toolbox section for more details](toolbox)) -->

* Git repository: https://gitlab.com/wurssb/NGTaxToolbox

## Mockrobiota analysis results

* The data can be found [here](https://gitlab.com/wurssb/ngtax/tree/master/data).