# NG-Tax 2.0: A Semantic Framework for High-throughput Amplicon Analysis

## Summary

![NG-Tax 2.0 workflow](../images/ngtax_workflow.png)

NG-Tax 2.0 is a semantic framework for FAIR high-throughput analysis and classification of marker gene amplicon sequences including bacterial and archaeal 16S ribosomal RNA (rRNA), eukaryotic 18S rRNA and ribosomal intergenic transcribed spacer sequences.
It can directly use single or merged reads, paired-end reads and unmerged paired-end reads from long range fragments as input to generate de novo Amplicon Sequence Variants (ASV).
Using the RDF data model, ASV’s can be automatically stored in a graph database as objects that link ASV sequences with the full data-wise and element-wise provenance, thereby achieving the level of interoperability required to utilize such data to its full potential.
The graph database can be directly queried, allowing for comparative analyses of over thousands of samples and is connected with an interactive Rshiny toolbox for analysis and visualization of (meta) data.
Additionally, NG-Tax 2.0 exports an extended BIOM 1.0 (JSON) file as starting point for further analyses by other means.
The extended BIOM file contains new attribute types to include information about the command arguments used, the sequences of the ASVs formed, classification confidence scores and is backwards compatible.

The performance of NG-Tax 2.0 was compared with DADA2, using the plugin in the QIIME 2 analysis pipeline.
Fourteen 16S rRNA gene amplicon mock community samples were obtained from the literature and evaluated.
Precision of NG-Tax 2.0 was significantly higher with an average of 0.95 vs 0.58 for QIIME2-DADA2 while recall was comparable with an average of 0.85 and 0.77, respectively.  

NG-Tax 2.0 is written in Java. The code, the ontology, a Galaxy platform implementation, the analysis toolbox, tutorials and example SPARQL queries are freely available at <http://wurssb.gitlab.io/ngtax> under the MIT License.

### Cite this article

Poncheewin W., Hermes, G.D.A. et al. NG-Tax 2.0: A Semantic Framework for High-throughput Amplicon Analysis. Frontiers in Genetics 10 (2019): 1366. [doi: 10.3389/fgene.2019.01366](https://doi.org/10.3389/fgene.2019.01366)
