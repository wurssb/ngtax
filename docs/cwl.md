# Workflow

## NG-Tax Workflow

There is a workflow written in CWL available for the NG-Tax pipeline. The workflow is available on the [WorkflowHub](https://workflowhub.eu/workflows/154) and can be executed using CWL. CWL is a standard for describing workflows and executing them on different platforms.

![](../images/ngtax-workflow.svg)

### Executing the workflow

To execute the NG-Tax workflow you need a CWL workflow runner. 

The easiest is to use cwltool and it can be installed using `pip install cwltool`.

To start the workflow you can use the following command

> cwltool https://git.wur.nl/unlock/cwl/-/raw/master/cwl/workflows/workflow_ngtax_picrust2.cwl

This will download the workflow and ensures that it can be executed. Be aware that the workflow makes use of docker containers and this is required to be installed on your system.

It is currently developed to process a single sample at a time. Parallelisation or library processing is not yet implemented.

#### Input

```
usage: https://git.wur.nl/unlock/cwl/-/raw/master/cwl/workflows/workflow_ngtax_picrust2.cwl [-h] [--destination DESTINATION] --for_read_len
                                                                                            FOR_READ_LEN --forward_primer FORWARD_PRIMER
                                                                                            --forward_reads FORWARD_READS --fragment FRAGMENT
                                                                                            [--metadata METADATA] [--primersRemoved]
                                                                                            [--reference_db REFERENCE_DB] [--rev_read_len REV_READ_LEN]
                                                                                            [--reverse_primer REVERSE_PRIMER]
                                                                                            [--reverse_reads REVERSE_READS] --sample SAMPLE
                                                                                            [--threads THREADS]
                                                                                            [job_order]
https://git.wur.nl/unlock/cwl/-/raw/master/cwl/workflows/workflow_ngtax_picrust2.cwl: error: the following arguments are required: --for_read_len, --forward_primer, --forward_reads, --fragment, --sample
```

#### Result

The outcome will be a folder with multiple files. The most important documents are related to:
- Quality control using fastqc
- The classification files in biom and RDF format
- Picrust annotation files
- Phyloseq TSV files
